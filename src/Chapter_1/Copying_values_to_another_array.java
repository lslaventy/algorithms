package Chapter_1;

public class Copying_values_to_another_array {
    public static void main(String[] args) {
        //исходные двнные
        double[] mas1 = {4, 5, 10, 4, 18};
        int len = mas1.length;
        double[] masCopy = new double[len];
        //копирование
        for (int i = 0; i < len; i++){
            masCopy[i] = mas1[i];
        }
        // System.arraycopy(mas1, 0, masCopy, 0, len);
        System.out.println("another_array");
        for (double d: masCopy) {
            System.out.print(d + " ");
        }
    }
}
