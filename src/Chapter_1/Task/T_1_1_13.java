package Chapter_1.Task;

import java.util.Random;
/**
 * Написать код для вывода транспозиции двумерного массива с М строками и N столбцами
 * (строки заменены столбцами)
 * */
public class T_1_1_13 {
    public static void main(String[] args) {
        //размерность массива
        final int N = 3, M = 3;
        //создание массива заданного размера
        int[][] a = intArr(N,M);
        //вывод на печать массива
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                System.out.print(a[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        //вывод на печать массива в транспонированном виде
        for (int i = 0 ; i  < N; i++ ){
            for (int j = 0; j < M; j++){
                System.out.print(a[j][i]);
            }
            System.out.println();
        }

    }

    public static int[][] intArr(int x, int y){
        //создание массива размера переданного в аргументах
        int[][] mass = new int[x][y];
        //инициализация объекта случайных чисел
        Random rnd = new Random();
        //строки массива
        for (int i = 0; i < x; i++) {
            //столбцы массива
            for (int j = 0; j < y; j++) {
                //запись случайной величины из списка bool
                mass[i][j] = rnd.nextInt(10);
            }
        }
        //возврат инициализированного массива
        return mass;
    }
}
