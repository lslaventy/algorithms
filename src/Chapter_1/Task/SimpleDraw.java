package Chapter_1.Task;



import static edu.princeton.cs.algs4.StdDraw.*;

public class SimpleDraw {
    public static void main(String[] args) {
//        line(0,0,1, 1);
//        setPenColor(Color.BLUE);
//        circle(0.5,0.5,0.3);
        int N = 100;
        setXscale(0, N);
        setYscale(0, N*N);
        setPenRadius(.01);
        for (int i=1; i<=N;i++){
            point(i, i);
            point(i, i*i);
            point(i, i*Math.log(i));
        }
    }
}
