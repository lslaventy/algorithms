package Chapter_1.Task;

import java.util.Random;
/**
 * Написать код, который выводит содержимое двумерного логического массива, используя звездочки
 * для представления значений true и пробел для false. Добавьте номера строк и столбцов.
 * */
public class T_1_1_11 {
    public static void main(String[] args) {
        //инициализация массива
        boolean[][] mass = boolArr(10, 10);
        //беру строку массива
        for (int i = 0; i < mass.length; i++){
            //если это первая строка
            if (i == 0) {
                //инициалация счетчика
                int a = i;
                //первый символ пробел
                System.out.print(" ");
                //выводится количество столбцов
                do {
                    System.out.print(a++);
                }while (a < mass.length);
                //перево строки
                System.out.println();
            }
            //вывод номера строки
            System.out.print(i);
            //столбец массива
            for (int j = 0; j < mass.length; j++){
                //проверка значения на true
                if (mass[i][j]){
                    //вывод звездочки
                    System.out.print("*");
                }else {
                    //иначе пробел
                    System.out.print(" ");
                }
            }
            //перевод строки после прохождения строки массива
            System.out.println();
        }
    }
    //метод инициализации массива
    public static boolean[][] boolArr(int x, int y){
        //создание массива размера переданного в аргументах
        boolean[][] mass = new boolean[x][y];
        //инициализация объекта случайных чисел
        Random rnd = new Random();
        //строки массива
        for (int i = 0; i < x; i++) {
            //столбцы массива
            for (int j = 0; j < y; j++) {
                //запись случайной величины из списка bool
                mass[i][j] = rnd.nextBoolean();
            }
        }
        //возврат инициализированного массива
        return mass;
    }
}
