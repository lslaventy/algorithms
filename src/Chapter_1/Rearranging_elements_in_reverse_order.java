package Chapter_1;

public class Rearranging_elements_in_reverse_order {
    public static void main(String[] args) {
        //Зададим массив
        double[] mas = {5, 7, 2, 1, 10, 11};
        int len = mas.length;
        for (int i = 0; i < len/2 ; i++){
            double tmp = mas[i];
            mas[i] = mas[len-i-1];
            mas[len-i-1] = tmp;
        }
        for (double res :
                mas) {
            System.out.print(res + " ");
        }

    }
}
