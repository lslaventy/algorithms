package Chapter_1;

public class Calculating_the_average_of_an_array {
    public static void main(String[] args) {
        //Зададим массив
        double[] mas = {5, 7, 2, 1, 10};
        double sum = 0;
        for (double ma : mas) {
            sum += ma;
        }
        System.out.println("average of an array is " +  sum/mas.length);
    }
}
