package Chapter_1;
public class Main {
    public static void main(String[] args) {
        System.out.println("test");
        int p = 10, q = 3;
        System.out.println("p = "+ p + ", q = " + q);
        System.out.println(gcd(p, q));


    }
    /** Алгоритм Евклида
     * Вычисляет наибольший общий делитель */
    public static int gcd(int p, int q){
        if (q == 0) return p;
        int r = p % q;
        return gcd(q, r);
    }
}
