package Chapter_1.Task;

import edu.princeton.cs.algs4.StdRandom;

public class T_1_1_1 {
    public static void main(String[] args) {
        System.out.println((0 + 15) / 2);
        System.out.println(2.0e-6 * 100000000.1);
        System.out.println(true && false || true && true);
        StdRandom.uniform();
        int a=0,b=9, c=0;
        double d = 15.50;
        System.out.printf("%14.4e", d);
    }
}