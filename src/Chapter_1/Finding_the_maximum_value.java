package Chapter_1;
/**Линейный поиск максимального значения в массиве*/
public class Finding_the_maximum_value {
    public static void main(String[] args) {
        //Зададим массив
        double[] mas = {5, 7, 2, 1, 10};
        //реализуем поиск
        double max = mas[0];
        for (double iteration : mas) if (iteration > max) max = iteration;
        //выводим результат
        System.out.println("maximum value is " + max);
    }
}
