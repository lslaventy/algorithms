package Chapter_1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class BinarySearch {
    public static int rank(int key, int[] a){
        //массив должен быть отсортирован
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi){
            //Key находится в a[lo..hi] или отсутствует.
            int mid = lo + (hi - lo) / 2;
            if      (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else                    return mid;
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(args[0]);
//        int[] whitelist = In.readInts(args[0]);
        ArrayList<Integer> list = new ArrayList<>();
        while (in.hasNext()){
            list.add(in.nextInt());
        }
        int[] whitelist = new int[list.size()];
        for (int i = 0; i< list.size(); i++){
            whitelist[i] = list.get(i);
        }
        Arrays.sort(whitelist);
        while (!StdIn.isEmpty()){
            //Чтение значения key и вывод, если его нет в белом списке.
            int key = StdIn.readInt();
            if (rank(key, whitelist) < 0){
                StdOut.println(key);
            }
        }

    }
}
